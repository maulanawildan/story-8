from django.test import TestCase
from django.test import Client
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import unittest
import time


class UnitTest(TestCase):
    def test_search_page_is_exist(self):
        self.response = Client().get('')
        self.assertEqual(self.response.status_code, 200)
    
    def test_search_page_is_not_exist(self):
        self.response = Client().get('error/')
        self.assertEqual(self.response.status_code, 404)
