Django==2.2.7
pytz==2019.3
selenium==3.141.0
sqlparse==0.3.0
urllib3==1.25.7
whitenoise==4.1.4
